nextflow.enable.dsl=2

/*
./nextflow src/solution_RNASeq.nf --fastq "data/tiny_dataset/fastq/tiny2_R{1,2}.fastq.gz" --fasta "data/tiny_dataset/fasta/tiny_v2_10.fasta" --bed "data/tiny_dataset/annot/tiny.bed" -profile docker
*/

params.fastq = "data/fastq/*.fastq.gz"
params.output = "results"
params.filter = "data/filter/*snRNA*.bt2"
params.fasta = "data/genome/*.fa"
params.gtf = "data/annotation/*.gtf"


log.info "input raw : ${params.fastq}"
log.info "outut directory : ${params.output}"
log.info "filter index files : ${params.filter}"
log.info "genome fasta : ${params.fasta}"
log.info "gtf file : ${params.gtf}"
log.info ""

Channel
   .fromPath ( params.fasta )
   .ifEmpty { error "Cannot find any index files matching: ${params.fasta}" }
   .set { GENOME_FASTA }
Channel
  .fromPath( params.fastq )
  .ifEmpty { error "Cannot find any fastq files matching: ${params.fastq}" }
  .map { it -> [(it.baseName =~ /([^\.]*)/)[0][1], it]}
  .set { FASTQ_FILES }
Channel
  .fromPath( params.filter )
  .ifEmpty { error "Cannot find any index files matching: ${params.filter}" }
  .set { FILTER_INDEX }
Channel
 .fromPath( params.gtf )
 .ifEmpty { error "Cannot find any gtf file matching: ${params.gtf}" }
 .set { GTF_FILE }


include { fastp_singleend } from './nf_modules/fastp'
include { bowtie_singleend } from './nf_modules/bowtie2'
include { index_fasta ; mapping_RP } from './nf_modules/star'
include { prepare_transcripts ; metaplots ; identification } from './nf_modules/ribocode'

workflow {
// trimming and QC analysis --------------------------------------------------------------------------
    fastp_singleend(FASTQ_FILES, params.output)

// Filtering -----------------------------------------------------------------------------------------
    bowtie_singleend(fastp_singleend.out.FASTQ, FILTER_INDEX.collect(), params.output)

// Index genome
    index_fasta(GENOME_FASTA, GTF_FILE)

// Mapping on genome and post genome -----------------------------------------------------------------
    mapping_RP(bowtie_singleend.out.FASTQ, index_fasta.out.INDEX_FILES.collect(), params.output)

// RiboCode prepare_transcripts -----------------------------------------------------------------
   prepare_transcripts(GENOME_FASTA, GTF_FILE, params.output)
   metaplots(prepare_transcripts.out.prepared_transcripts.collect(), mapping_RP.out.BAM_TRANSCRIPTOME, params.output)

   mapping_RP.out.BAM_TRANSCRIPTOME.join(metaplots.out.CONFIG_FILE)
                     .set{MERGED_BAM_CONFIG}

   identification(prepare_transcripts.out.prepared_transcripts.collect(), MERGED_BAM_CONFIG, params.output)
}
