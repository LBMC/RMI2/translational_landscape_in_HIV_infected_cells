#!/bin/bash
set -e

nextflow src/workflow_consensus_bam.nf -resume \
				                           -profile docker \
                                       --bam "results/RP_SupT1_HIV2/03_mapping_postgenome/RP_SupT1_HIV2_all_postgenome.bam" \
                                       --output "results/RP_SupT1_HIV2/ROD_consensus" \
                                       --fasta "data/post_genome/X05291/*.fa"
