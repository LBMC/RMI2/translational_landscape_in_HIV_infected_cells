#!/usr/bin/env bash

set -e

nextflow src/workflow_RibosomeProfiling.nf -profile psmn \
                                         -resume \
                                         --fastq "data/fastq/RP_HTLV/*.fastq.gz"\
                                         --output "results/RP_HTLV"\
                                          --filter "data/filter/human_rRNA_tRNA_snRNA*.bt2"\
                                          --index_genome "data/genome/*.ht2"\
                                          --index_postgenome "data/post_genome/HTLV_1/*.ht2"\
                                          --gtf "data/annotation/Homo_sapiens.GRCh38.84.gtf"\
                                          --gtf_postgenome "data/annotation/NY5_CDS.gff"

nextflow src/workflow_RibosomeProfiling.nf -profile psmn \
                                         -resume \
                                         --fastq "data/fastq/RP_SupT1/HIV2/*.fastq.gz"\
                                         --output "results/RP_SupT1_HIV2"\
                                          --filter "data/filter/human_rRNA_tRNA_snRNA*.bt2"\
                                          --index_genome "data/genome/*.ht2"\
                                          --index_postgenome "data/post_genome/X05291/*.ht2"\
                                          --gtf "data/annotation/Homo_sapiens.GRCh38.84.gtf"\
                                          --gtf_postgenome "data/annotation/NY5_CDS.gff"

