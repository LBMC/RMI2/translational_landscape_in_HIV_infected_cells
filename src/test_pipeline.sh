#!/usr/bin/env bash

set -e

nextflow src/workflow_RibosomeProfiling.nf -profile docker \
                                         -resume \
                                         --fastq "data/fastq/RP_SupT1/*HIV1*36h*rep3*.fastq.gz"\
                                         --output "results/RP_test"\
                                          --filter "data/filter/human_rRNA_tRNA_snRNA*.bt2"\
                                          --index_genome "data/genome/*.ht2"\
                                          --index_postgenome "data/post_genome/M38431.1/*.ht2"\
                                          --gtf "data/annotation/Homo_sapiens.GRCh38.84.gtf"\
                                          --gtf_postgenome "data/annotation/NY5_CDS.gff"\
					  --do_dedup false

