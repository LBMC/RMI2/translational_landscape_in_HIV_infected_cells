#!/usr/bin/env bash

set -e

nextflow src/workflow_RibosomeProfiling.nf -profile psmn \
                                         -resume \
                                         --fastq "data/fastq/RP_HEK_LTRmutants_RNA/*.fastq.gz"\
                                         --output "results/RP_HEK_LTRmutants_RNA/"\
                                          --filter "data/filter/human_rRNA_tRNA_snRNA*.bt2"\
                                          --index_genome "data/genome/*.ht2"\
                                          --index_postgenome "data/post_genome/LTR0_Ren/*.ht2"\
                                          --gtf "data/annotation/Homo_sapiens.GRCh38.84.gtf"\
                                          --gtf_postgenome "data/annotation/NY5_CDS.gff"

