version = "0.5.1--py37h190e900_3"
container = "quay.io/biocontainers/plastid:${version}"


process plastid_generate {
  container = "${container}"
  tag "plastid_generate"
  label "big_mem_mono_cpus"
  publishDir "${output}/plastid/annot", mode: 'copy'

  input:
  path (gtf)
  val (landmark)
  val (downstream)
  val (output)

  output:
  path ("*.txt"), emit : ANNOTED_FILES

  script:
"""
metagene generate ${gtf.baseName} \
       --landmark ${landmark} \
       --annotation_files ./${gtf} \
       --downstream ${downstream}

"""
}

process plastid_psite {
  container = "${container}"
  tag "${bam.baseName}"
  label "big_mem_mono_cpus"
  publishDir "${output}/plastid/psite", mode: 'copy'

  input:
  path (ROI)
  tuple val(file_id), path(bam), path(bai)
  val (min_length)
  val (max_length)
  val (output)

  output:
  path "*", emit : P_SITE 

  script:
"""
psite ${ROI} ${bam.baseName} \
             --min_length ${min_length} \
             --max_length ${max_length} \
             --require_upstream \
             --count_files ${bam} 
"""
}



process plastid_metagene {
  tag "${bam.baseName}"
  container = "${container}"
  label "big_mem_mono_cpus"
  publishDir "${output}/plastid/metagene", mode: 'copy'

  input:
  tuple val(file_id), path(bam), path(bai)
  path(roi)
  val(min_counts)
  val(output)

  output:
  tuple val(file_id), path( "*metagene_profile.txt"), emit: METAGENE_FILES
  path "*", emit : OTHER_FILES

  script:
"""
metagene count ${roi} ${file_id} \
               --count_files ${bam}\
               --fiveprime \
               --offset 11 \
               --normalize_over 30 200 \
               --min_counts ${min_counts} \
               --cmap Blues \
               --title "${file_id}"

"""
}

process plastid_chart {
  tag "${file_id}"
  container = "${container}"
  label "big_mem_mono_cpus"
  publishDir "${output}/plastid/chart", mode: 'copy'

  input:
  tuple val(file_id), path (metagene_profile)
  val(landmark)
  val(output)

  output:
  path "*", emit : CHART_files

  script:
"""
metagene chart --landmark "${landmark}" \
               --title "metagene plot"\
               Metachart_${file_id}\
               *metagene_profile.txt

"""
}
