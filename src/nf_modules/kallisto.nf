version = "0.46.2--h60f4f9f_2"
container_url = "quay.io/biocontainers/kallisto:${version}"

process index_fasta {
  container = "${container_url}"
  label "big_mem_multi_cpus"
  publishDir "data/transcriptome/kallisto_index"
  tag "$fasta.baseName"

  input:
    path fasta

  output:
    path "*.index*", emit: index
    path "*_report.txt", emit: report

  script:
"""
kallisto index -k 31 --make-unique -i ${fasta.baseName}.index ${fasta} \
2> ${fasta.baseName}_kallisto_index_report.txt
"""
}


process mapping_fastq_pairedend {
  container = "${container_url}"
  label "big_mem_multi_cpus"
  tag "$pair_id"
  publishDir "${output}/mapping", mode : 'copy', pattern: "*"


  input:
  path index
  tuple val(pair_id), path(reads)
  val output

  output:
  path "${pair_id}", emit: counts
  path "*_report.txt", emit: report

  script:
"""
mkdir ${pair_id}
kallisto quant -i ${index} -t ${task.cpus} \
--bias --bootstrap-samples 100 -o ${pair_id} \
----fr-stranded \
${reads[0]} ${reads[1]} &> ${pair_id}_kallisto_mapping_report.txt
"""
}


process mapping_fastq_singleend {
  container = "${container_url}"
  label "big_mem_multi_cpus"
  tag "$file_id"

  input:
  path index
  tuple val(file_id), path(reads)

  output:
  tuple val(file_id), path("${pair_id}"), emit: counts
  path "*_report.txt", emit: report

  script:
"""
mkdir ${file_id}
kallisto quant -i ${index} -t ${task.cpus} --single \
--bias --bootstrap-samples 100 -o ${file_id} \
-l ${params.mean} -s ${params.sd} \
${reads} &> ${reads.simpleName}_kallisto_mapping_report.txt
"""
}
