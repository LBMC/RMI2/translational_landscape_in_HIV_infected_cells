version = "0.2.5--pyh864c0ab_1"
container = "quay.io/biocontainers/ribotish:${version}"


process quality_CHX {
  tag "$file_id"
  container = "${container}"
  label "big_mem_multi_cpus"
  publishDir "$output/ribotish/quality", mode: 'copy'

  input:
    path gtf
    tuple val(file_id), path(bam), path(bai)
    val output

  output:
    path "*.pdf", emit : PDF
    tuple val(file_id), path("*.txt"), emit : QUALITY_FILE

  script:
"""
ribotish quality -b ${bam} -g ${gtf} --th 0.42 -p ${task.cpus} -l 25,37
"""
}

process quality_HAR {
  tag "$file_id"
  container = "${container}"
  label "big_mem_multi_cpus"
  publishDir "$output/ribotish/quality", mode: 'copy'

  input:
    path gtf
    tuple val(file_id), path(bam), path(bai)
    val output

  output:
    path "*.pdf", emit : PDF
    tuple val(file_id), path("*.txt"), emit : QUALITY_FILE

  script:
"""
ribotish quality -b ${bam} -g ${gtf} -t --th 0.42 -p ${task.cpus} -l 25,37
"""
}

process prediction_CHX {
  tag "$file_id"
  container = "${container}"
  label "big_mem_multi_cpus"
  publishDir "$output/ribotish/prediction", mode: 'copy'

  input:
    path gtf
    path genome
    tuple val(file_id), path(bam), path(bai), path(quality)
    val output

  output:
    tuple val(file_id), path("*"), emit : PREDICTION 

  script:
"""
ribotish predict -b ${bam} -g ${gtf} -f ${genome} -o ${file_id}_pred.txt -p ${task.cpus} --alt
"""
}

process prediction_HAR{
  tag "$file_id"
  container = "${container}"
  label "big_mem_multi_cpus"
  publishDir "$output/ribotish/prediction", mode: 'copy'

  input:
    path gtf
    path genome
    tuple val(file_id), path(bam), path(bai), path(quality)
    val output

  output:
    tuple val(file_id), path("*"), emit : PREDICTION

  script:
"""
ribotish predict -t ${bam} -g ${gtf} -f ${genome} -o ${file_id}_pred.txt -p ${task.cpus} --alt
"""
}
