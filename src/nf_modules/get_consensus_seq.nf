version = "1.7"
container = "labaronne/bcftools_samtools:${version}"

process get_consensus_from_bam {
  container = "${container}"
  label "big_mem_mono_cpus"
  tag "$file_id"
  publishDir "${output}/seq_consensus/", mode: 'copy'

   input:
   tuple val(file_id), file(alignment)
   file(genome)
   val(output)

   output:
   tuple val(file_id), file("*consensus.fa"), emit : CONSENSUS

   script:
   """
   # sort and index bam
   samtools sort -o ${file_id}_sort.bam ${alignment}
   samtools index ${file_id}_sort.bam

   # call variant
   bcftools mpileup -Ou -f ${genome} ${file_id}_sort.bam | bcftools call -Oz -mv -o ${file_id}.vcf.gz
   bcftools index ${file_id}.vcf.gz

   # get consensus from variant
   cat ${genome} | bcftools consensus ${file_id}.vcf.gz > ${file_id}_${genome.baseName}_consensus.fa
   """
}
