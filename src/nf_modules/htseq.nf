version = "0.11.2"
container = "lbmc/htseq:${version}"

process counting_exon {
container = "${container}"
tag "$file_id"
label "big_mem_mono_cpus"
publishDir "$output/04_HTseq", mode: 'copy', pattern: "*"

input:
tuple val(file_id), file(bam)
file(gtf)
val(output)

output:
tuple val(file_id), path ("*.count"), emit : COUNT

script:
"""
htseq-count ${bam} ${gtf} \
    --mode=intersection-nonempty \
    -a 10 \
    -s yes \
    -t exon \
    -i gene_id \
    -f bam \
> ${file_id}_${gtf.baseName}.count

"""
}

process counting_CDS {
container = "${container}"
tag "$file_id"
label "big_mem_mono_cpus"
publishDir "$output/04_HTseq", mode: 'copy', pattern: "*"

input:
tuple val(file_id), file(bam)
file(gtf)
val(output)

output:
tuple val(file_id), path ("*.count"), emit : COUNT

script:
"""
htseq-count ${bam} ${gtf} \
    --mode=intersection-nonempty \
    -a 10 \
    -s yes \
    -t CDS \
    -i gene_id \
    -f bam \
> ${file_id}_${gtf.baseName}_CDS.count
"""
}

process merging_counts {
tag "$file_id"
label "big_mem_mono_cpus"
publishDir "$output/04_HTseq_merged", mode: 'copy', pattern: "*"

input:
tuple val(file_id), file(genome), file(postgenome)
val(output)

output:
path ("*.count"), emit : COUNT_MERGED

script:
"""
cat ${genome} >> ${file_id}.count
cat ${postgenome} >> ${file_id}.count
"""
}

process merging_counts_CDS {
tag "$file_id"
label "big_mem_mono_cpus"
publishDir "$output/04_HTseq_merged", mode: 'copy', pattern: "*"

input:
tuple val(file_id), file(genome), file(postgenome)
val(output)

output:
path ("*.count"), emit : COUNT_MERGED

script:
"""
cat ${genome} >> ${file_id}_CDS.count
cat ${postgenome} >> ${file_id}_CDS.count
"""
}
