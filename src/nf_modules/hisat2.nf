version = "2.1.0"
container = "lbmc/hisat2:${version}"

process mapping_paired {
  container = "${container}"
  tag "$pair_id"
  label "big_mem_multi_cpus"
  publishDir "$output/02_mapping_genome", mode: 'copy', pattern: "*.{bam,bai,txt}"

  input:
  tuple val(pair_id), file(fastq)
  file(index)
  val(output)

  output:
  tuple val(pair_id), file("*.bam"), emit: BAM
  tuple val(pair_id), file("*.bai"), emit: BAI
  path("*_report.txt"), emit: LOGS
  tuple val(pair_id), file("*R{1,2}.fastq.gz"), emit : FASTQ

  script:
  index_id = index[0]
  for (index_file in index) {
    if (index_file =~ /.*\.1\.ht2/ && !(index_file =~ /.*\.rev\.1\.ht2/)) {
        index_id = ( index_file =~ /(.*)\.1\.ht2/)[0][1]
    }
  }
"""
hisat2 -x ${index_id} \
       -p ${task.cpus} \
       -1 ${fastq[0]} \
       -2 ${fastq[1]} \
       --un-conc-gz ${pair_id}_notaligned_R%.fastq.gz \
       --rna-strandness 'FR' \
       --dta \
       --no-softclip\
       --trim3 1\
       --trim5 1\
       2> ${pair_id}_report.txt \
| samtools view -bS -F 4 - \
| samtools sort -@ ${task.cpus} -o ${pair_id}_genome.bam \
&& samtools index ${pair_id}_genome.bam

if grep -q "ERR" ${pair_id}_report.txt; then
  exit 1
fi
"""
}

process mapping_paired2 {
  container = "${container}"
  tag "$pair_id"
  label "big_mem_multi_cpus"
  publishDir "$output/03_mapping_postgenome", mode: 'copy', pattern: "*.{bam,bai,txt}"

  input:
  tuple val(pair_id), file(fastq)
  file(index)
  val(output)

  output:
  tuple val(pair_id), file("*.bam"), emit: BAM
  tuple val(pair_id), file("*.bai"), emit: BAI
  path("*_report.txt"), emit: LOGS

  script:
  index_id = index[0]
  for (index_file in index) {
    if (index_file =~ /.*\.1\.ht2/ && !(index_file =~ /.*\.rev\.1\.ht2/)) {
        index_id = ( index_file =~ /(.*)\.1\.ht2/)[0][1]
    }
  }
"""
hisat2 -x ${index_id} \
       -p ${task.cpus} \
       -1 ${fastq[0]} \
       -2 ${fastq[1]} \
       --rna-strandness 'FR' \
       --dta \
       --no-softclip\
       --trim3 1\
       --trim5 1\
       2> ${pair_id}_report.txt \
| samtools view -bS -F 4 - \
| samtools sort -@ ${task.cpus} -o ${pair_id}_postgenome.bam \
&& samtools index ${pair_id}_postgenome.bam

if grep -q "ERR" ${pair_id}_report.txt; then
  exit 1
fi
"""
}

process mapping_single {
   container = "${container}"
  tag "$file_id"
  label "big_mem_multi_cpus"
  publishDir "$output/02_mapping_genome", mode: 'copy', pattern: "*.{bam,bai,txt}"

  input:
  tuple val(file_id), file(fastq)
  file(index)
  val(output)

  output:
  tuple val(file_id), file("*.bam"), emit: BAM
  tuple val(file_id), file("*.bai"), emit: BAI
  path("*.txt"), emit: LOGS
  tuple val(file_id), file("*.fastq.gz"), emit : FASTQ

  script:
  index_id = index[0]
  for (index_file in index) {
    if (index_file =~ /.*\.1\.ht2/ && !(index_file =~ /.*\.rev\.1\.ht2/)) {
        index_id = ( index_file =~ /(.*)\.1\.ht2/)[0][1]
    }
  }
  """
  hisat2 -x ${index_id} \
         -p ${task.cpus} \
         -U ${fastq[0]} \
         --un-gz ${file_id}_notaligned.fastq.gz \
         --rna-strandness 'F' \
         --dta \
         --no-softclip \
         --trim5 1\
         --trim3 1\
         2> ${file_id}_genome.txt \
  | samtools view -bS -F 4 - \
  | samtools sort -@ ${task.cpus} -o ${file_id}_genome.bam \
  && samtools index ${file_id}_genome.bam

  if grep -q "ERR" ${file_id}.txt; then
    exit 1
  fi
  """
}

process mapping_single2 {
   container = "${container}"
  tag "$file_id"
  label "big_mem_multi_cpus"
  publishDir "$output/03_mapping_postgenome", mode: 'copy', pattern: "*.{bam,bai,txt}"

  input:
  tuple val(file_id), file(fastq)
  file(index)
  val(output)

  output:
  tuple val(file_id), file("*.bam"), emit: BAM
  tuple val(file_id), file("*.bai"), emit: BAI
  path("*.txt"), emit: LOGS

  script:
  index_id = index[0]
  for (index_file in index) {
    if (index_file =~ /.*\.1\.ht2/ && !(index_file =~ /.*\.rev\.1\.ht2/)) {
        index_id = ( index_file =~ /(.*)\.1\.ht2/)[0][1]
    }
  }
  """
  hisat2 -x ${index_id} \
         -p ${task.cpus} \
         -U ${fastq[0]} \
         --rna-strandness 'F' \
         --dta \
         --no-softclip \
         --trim5 1\
         --trim3 1\
         2> ${file_id}_postgenome.txt \
  | samtools view -bS -F 4 -F 16 - \
  | samtools sort -@ ${task.cpus} -o ${file_id}_postgenome.bam \
  && samtools index ${file_id}_postgenome.bam

  if grep -q "ERR" ${file_id}.txt; then
    exit 1
  fi
  """
}

process index_bam {
  container = "${container}"
  tag "$file_id"
  label "big_mem_mono_cpus"

  input :
  tuple val(file_id), path(bam)

  output:
  tuple val(file_id), path("*.bai"), emit : INDEX_FILE

  script:

"""
samtools index ${bam}
"""
}

process merge_bam {
  container = "${container}"
  tag "$file_id"
  label "big_mem_multi_cpus"

  input :
  tuple val(file_id), path(bam)

  output:
  tuple val(file_id), path("*sorted.bam"), emit : MERGED_BAM

  script:

"""
samtools merge -@ ${task.cpus} ${file_id}_all_merged.bam *.bam
samtools sort -o ${file_id}_all_merged_sorted.bam ${file_id}_all_merged.bam
"""
}
