version = "1.2.11--pyh145b6a8_1"
container = "quay.io/biocontainers/ribocode:${version}"


process prepare_transcripts {
  tag "$fasta.baseName"
  container = "${container}"
  label "big_mem_mono_cpus"
  publishDir "$output/ribocode/prepare_transcripts", mode: 'copy'

  input:
    path fasta
    path gtf
    val output

  output:
    path "$fasta.baseName/*", emit : prepared_transcripts

  script:
"""
prepare_transcripts  -g ${gtf} \
                     -f ${fasta} \
                     -o $fasta.baseName

"""
}

process metaplots {
  tag "$file_id"
  container = "${container}"
  label "big_mem_mono_cpus"
  publishDir "$output/ribocode/metaplots", mode: 'copy'

  input:
    path annot
    tuple val(file_id), path(bam)
    val output

  output:
    path "*.pdf", emit : PDF
    tuple val(file_id), path("*config.txt"), emit : CONFIG_FILE

  script:
"""
mkdir prepared_transcript_files
mv ${annot} prepared_transcript_files

metaplots   -a prepared_transcript_files \
            -r ${bam} \
            --stranded yes\
	    --maximum-length 38\
	    -pv1 0.05 \
	    -pv2 0.05 \
	    -f0_percent 0.5\
            --outname $file_id
"""
}

process identification {
  tag "$file_id"
  container = "${container}"
  label "big_mem_multi_cpus"
  publishDir "$output/ribocode/ORF_dectection", mode: 'copy'

  input:
    path annot
    tuple val(file_id), path(bam), path(config)
    val output

  output:
    path "*", emit : PREDICTION 

  script:
"""
mkdir prepared_transcript_files
mv ${annot} prepared_transcript_files

RiboCode   -a prepared_transcript_files \
           -c $config \
	   --longest-orf no\
           --alt_start_codons CTG,GTG,TTG,ACG,AAG,AGG,ATA,ATT,ATC \
	   --min-AA-length 5\
           -o $file_id
"""
}

