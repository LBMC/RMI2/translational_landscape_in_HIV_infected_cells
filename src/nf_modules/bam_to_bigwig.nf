version = "2.1.0"
container = "lbmc/hisat2:${version}"

process calc_scalingFactor_with_postgenome {
  container = "${container}"
  label "big_mem_mono_cpus"
  tag "$file_id"

   input:
   tuple val(file_id), file(genome), file(genome_bai), file(post_genome), file(post_genome_bai)

   output:
   tuple val(file_id), env(genome), env(postgenome), env(factor), emit : FACTOR

   shell:
   '''
   genome=$(samtools view !{genome[0]} | awk '{print $1}' | sort | uniq | wc -l)
   postgenome=$(samtools view !{post_genome[0]} | awk '{print $1}' | sort | uniq | wc -l)
   total=$(($genome + $postgenome))
   factor=$(awk -v c=$total 'BEGIN {print 1000000/c}')
   '''
}


version2 = "3.5.1--py_0"
container2 = "quay.io/biocontainers/deeptools:${version2}"

process coverage_with_postgenome{
   container = "${container2}"
   label "big_mem_multi_cpus"
   tag "$file_id"
   publishDir "${output}/07_coverage/", mode: 'copy'

   input:
   tuple val(file_id), val(genome), val(postgenome), val (factor),  file(genome_bam), file(genome_bai), file(post_genome_bam), file(post_genome_bai)
   val(output)

   output:
   path("*.bigwig"), emit: COVERAGE
   path("*.txt"), emit: LOG

   shell:
   '''
   total=$((!{genome} + !{postgenome}))
   echo "genome aligment : !{genome} counts" >> !{file_id}.txt
   echo "postgenome aligment : !{postgenome} counts" >> !{file_id}.txt
   echo "total counts : $total" >> !{file_id}.txt
   echo "scaling factor : !{factor}" >> !{file_id}.txt
   if [ !{genome} -gt 0 ]
   then
     bamCoverage -p !{task.cpus} --binSize 1  --scaleFactor !{factor} -b !{genome_bam[0]} -o !{file_id}_genome.bigwig
   fi
   if [ !{postgenome} -gt 0 ]
   then
     bamCoverage -p !{task.cpus} --binSize 1  --scaleFactor !{factor} -b !{post_genome_bam[0]} -o !{file_id}_postgenome.bigwig
   fi
   '''
}
