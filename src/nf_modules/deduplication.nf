version = "2.1.0"
container = "lbmc/hisat2:${version}"

process dedup {
  container = "${container}"
  label "big_mem_mono_cpus"
  tag "$file_id"
  publishDir "${output}/02_mapping_genome_dedup", mode: 'copy', pattern: "*.{log,bam,bai}"

  input:
  tuple val(file_id), file(bam)
  val output

  output:
    tuple val(file_id), path("*.bam"), emit: BAM
    tuple val(file_id), path("*.bai"), emit: BAI
    tuple val(file_id), path("*.log"), emit: LOG

  shell:
 '''
 samtools view -h !{bam[0]} | awk '!seen[substr($1, length($1)-5) $3 $4 $8 $9 $10]++' | samtools view -bS -o !{file_id}_dedup.bam
 samtools index !{file_id}_dedup.bam
 input=$(samtools view -h !{bam[0]} | awk '!seen[substr($1, 1,  length($1)-7)]++' | wc -l)
 output=$(samtools view -h !{bam[0]} | awk '!seen[substr($1, 1,  length($1)-7)]++' | wc -l)
 diff=$(($input - $output))
 per=$(($diff * 100 / $input))
 echo "Input : $input reads" > !{file_id}_dedup.log
 echo "Output : $output reads" >> !{file_id}_dedup.log
 echo "$per % duplicated reads" >> !{file_id}_dedup.log
 '''
}

process dedup_postgenome {
  container = "${container}"
  label "big_mem_mono_cpus"
  tag "$file_id"
  publishDir "${output}/03_mapping_postgenome_dedup", mode: 'copy', pattern: "*.{log,bam,bai}"

  input:
  tuple val(file_id), file(bam)
  val output

  output:
    tuple val(file_id), path("*.bam"), emit: BAM
    tuple val(file_id), path("*.bai"), emit: BAI
    tuple val(file_id), path("*.log"), emit: LOG

  shell:
 '''
 samtools view -h !{bam[0]} | awk '!seen[substr($1, length($1)-5) $3 $4 $8 $9 $10]++' | samtools view -bS -o !{file_id}_postgenome_dedup.bam
 samtools index !{file_id}_postgenome_dedup.bam
 input=$(samtools view -h !{bam[0]} | awk '!seen[substr($1, 1,  length($1)-7)]++' | wc -l)
 output=$(samtools view -h !{bam[0]} | awk '!seen[substr($1, 1,  length($1)-7)]++' | wc -l)
 diff=$(($input - $output))
 per=$(($diff * 100 / $input))
 echo "Input : $input reads" > !{file_id}_postgenome_dedup.log
 echo "Output : $output reads" >> !{file_id}_postgenome_dedup.log
 echo "$per % duplicated reads" >> !{file_id}_postgenome_dedup.log
 '''
}
