version = "2.7.3a"
container = "lbmc/star:${version}"


process index_fasta {
  tag "$fasta.baseName"
  container = "${container}"
  label "big_mem_multi_cpus"
  publishDir "data/genome/star", mode: 'copy'

  input:
    path fasta
    path gtf

  output:
    path "*", emit:  INDEX_FILES

  script:
"""
STAR --runThreadN ${task.cpus} --runMode genomeGenerate \
--genomeDir ./ \
--genomeFastaFiles ${fasta} \
--sjdbGTFfile ${gtf}
"""
}

process mapping_RP {
  container = "${container}"
  tag "$file_id"
  label "big_mem_multi_cpus"
  publishDir "$output/STAR/mapping", mode: 'copy'

  input:
    tuple val(file_id), path(fastq)
    path index
    val output

  output:
    tuple val(file_id), path("*Aligned.toTranscriptome.out.bam"), emit:  BAM_TRANSCRIPTOME
    tuple val(file_id), path("*Aligned.sortedByCoord.out.bam"), emit:  BAM_GENOME

  script:
"""
mkdir -p index
mv ${index} index/

STAR  --outFilterType BySJout \
      --runThreadN ${task.cpus} \
      --outFilterMismatchNmax 2 \
      --genomeDir index/ \
      --readFilesCommand zcat\
      --readFilesIn ${fastq} \
      --outFileNamePrefix ${file_id} \
      --outSAMtype BAM SortedByCoordinate \
      --quantMode TranscriptomeSAM GeneCounts \
      --outSAMattributes All \
      --outFilterMultimapNmax 1 \
      --outFilterMatchNmin 16 \
      --alignEndsType EndToEnd

"""
}
