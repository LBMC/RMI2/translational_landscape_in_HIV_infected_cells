version = "1.3.2--py_0"
container = "quay.io/biocontainers/ribotricer:${version}"


process prepare_transcripts {
  tag "$fasta.baseName"
  container = "${container}"
  label "big_mem_mono_cpus"
  publishDir "$output/ribotricer/annotation", mode: 'copy'

  input:
    path fasta
    path gtf
    val output

  output:
    path "*_candidate_orfs.tsv", emit : CANDIDATE_ORFS

  script:
"""
ribotricer prepare-orfs --gtf ${gtf} \
                        --fasta ${fasta} \
                        --prefix $fasta.baseName \
			--min_orf_length 10 \
			--start_codons ATG,CTG,GTG,TTG,AAG,AGG,ACG,ATT,ATA,ATC

"""
}

process detect_orfs {
  tag "$file_id"
  container = "${container}"
  label "big_mem_multi_cpus"
  publishDir "$output/ribotricer/detect_orfs", mode: 'copy'

  input:
    path candidate_orfs
    tuple val(file_id), path(bam)
    val output

  output:
    tuple val(file_id), path("*"), emit : CONFIG_FILE

  script:
"""
ribotricer detect-orfs \
             --bam ${bam} \
             --ribotricer_index ${candidate_orfs} \
             --prefix ${file_id}
"""
}
