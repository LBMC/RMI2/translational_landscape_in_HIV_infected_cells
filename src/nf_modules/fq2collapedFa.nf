version = "0.0.1"
container = "labaronne/fq2collapedfa:${version}"


process fq2collapedfa {
  tag "$file_id"
  container = "${container}"
  label "big_mem_mono_cpus"
  publishDir "$output/collapedFa/", mode: 'copy'

  input:
    tuple val(file_id), path(fastq)
    val output

  output:
    path "*.fa.gz", emit : COLLAPEDFA

  script:
"""
 perl /fq2collapedFa.pl -i ${fastq} -o ${file_id}.fa.gz
 """
}
