version = "1.9"
container_url = "lbmc/multiqc:${version}"

process multiqc {
  container = "${container_url}"
  label "big_mem_mono_cpus"
  publishDir "$output/multiqc", mode: 'copy', pattern: "*"

  input:
    path report
    val output

  output:
    path "*multiqc_*", emit: report

  script:
"""
multiqc -f .
"""
}
