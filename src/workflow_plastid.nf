nextflow.enable.dsl=2

params.landmark = "cds_start"
params.downstream = 120
params.gtf = "data/*.gtf"
params.output = "results"
params.bam = "data/*.bam"
params.min_length = 26
params.max_length = 32
params.min_counts = 50

log.info "gtf file : ${params.gtf}"
log.info "bam files : ${params.bam}"
log.info "outut directory : ${params.output}"
log.info "downstream CDS : ${params.downstream}"
log.info "min_length of footprints : ${params.min_length}"
log.info "max length of footprints : ${params.max_length}"
log.info ""

Channel
.fromPath( params.gtf )
.ifEmpty { error "Cannot find any gff files matching: ${params.gtf}" }
.set { ANNOT_FILES }
Channel
  .fromPath( params.bam )
  .ifEmpty { error "Cannot find any files matching: ${params.bam}" }
  .map { file -> tuple(file.baseName, file) }  
  .set { BAM_FILES }


include { plastid_generate } from './nf_modules/plastid'
include { plastid_psite} from './nf_modules/plastid'
include { index_bam} from './nf_modules/hisat2.nf'
include { plastid_metagene } from './nf_modules/plastid'
include { plastid_chart} from './nf_modules/plastid'

workflow {
   plastid_generate(ANNOT_FILES, params.landmark, params.downstream, params.output)
   index_bam(BAM_FILES)
   BAM_FILES.join(index_bam.out.INDEX_FILE)
  	    .set{BAM_INDEXED}
   plastid_psite(plastid_generate.out.ANNOTED_FILES.collect(), 
		BAM_INDEXED, 
		params.min_length, 
		params.max_length,
		params.output)
   plastid_metagene(BAM_INDEXED, 
		    plastid_generate.out.ANNOTED_FILES.collect(),
		    params.min_counts, 
		    params.output)
   plastid_chart(plastid_metagene.out.METAGENE_FILES,
		 params.landmark,
		 params.output)
}
