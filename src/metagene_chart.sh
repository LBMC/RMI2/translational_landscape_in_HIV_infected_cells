#!/bin/bash

set -e

metagene chart 	--landmark "cds_start" \
		--title "U937 RNAseA+T1 CHX and HAR" \
		--figformat svg \
		results/plastid/Metachart_U937.png \
		results/plastid/metagene/RP_U937_*profile.txt

metagene chart 	--landmark "cds_start" \
		--title "SupT1 CHX MNase and RNAseI" \
		--figformat svg \
		results/plastid/Metachart_SupT1.png \
		results/plastid/metagene/RP_SupT1*profile.txt
