solarGrey01 <- rgb(88,110,117,maxColorValue=255)
solarBlack <- rgb(0, 43, 54, maxColorValue=255)
solarMagenta <- rgb(211,54,130,maxColorValue=255)
solarRed <- rgb(220,50,47,maxColorValue=255)
solarOrange <- rgb(203, 75, 22, maxColorValue=255)
solarYellow <- rgb(181,137,0,maxColorValue=255)
solarGreen <- rgb(133,153,0,maxColorValue=255)
solarCyan <- rgb(42,161,152,maxColorValue=255)
solarBlue <- rgb(38,139,210,maxColorValue=255)
solarViolet <- rgb(108,113,196,maxColorValue=255)

lighten <- function(corig,lfact=0.6) {
  crgb <- col2rgb(corig)
  clight <- 255 - ((255 - crgb) * lfact)
  rgb(clight["red",], clight["green",], clight["blue",], maxColorValue=255)
}

minor_decade <- function(d) { seq(2*d, 9*d, d) }

