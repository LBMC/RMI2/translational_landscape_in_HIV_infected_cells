library("GenomicRanges")
library("Rsamtools")
library("GenomicAlignments")

## Sample asiteOffests
## asites <- data.frame(asite=c(14,14,14,14,15,15), row.names=seq(26,31))
## asite
##  26    14
##  27    14
##  28    14
##  29    14
##  30    15
##  31    15
    
# Using a data frame of A site offsets, convert a GAlignments of footprint
# alignments into a GRanges of A site nucleotides still in genomic coordinates.
alignASites <- function(asiteOffsets, alns) {
    alnLengths <- qwidth(alns)
    alnAOffs <- asiteOffsets[as.character(alnLengths), "asite"]

    ## e.g., 30mer runs 101 to 130 and offset is +14
    ##   (+) strand, A site is 115
    ##     start = 15 = offset + 1
    ##   (-) strand, A site is 116
    ##     offset from start is 30 - 14 = 16
    alnDelta <- ifelse(strand(alns) == "-", alnLengths - alnAOffs, alnAOffs + 1)
                       
    alnsGood <- alns[!is.na(alnDelta)]
    alnDelta <- alnDelta[!is.na(alnDelta)]

    if (length(alnsGood) > 0) {
        qnarrow(alnsGood, start = alnDelta, width = 1)
    } else {
        GRanges()
    }
}

getAlignASites <- function(asiteOffsets, bamfile, trx) {
  alignASites(asiteOffsets, getAlignments(bamfile, trx))
}

getASiteProfile <- function(asiteOffsets, bamfile, trx) {
  asites <- getAlignASites(asiteOffsets, bamfile, trx)

  if (length(asites) > 0) { 
    tabulate( relativeWithin(start(asites), trx),
             nbins = sum(width(trx)) )
  } else {
    tabulate( as.integer(c()), nbins = sum(width(trx)) )
  }
}

# Insets in nucleotides for avoiding start and stop
defaultInsets <- list( utr5Inset3 = 6, cdsInset5 = 45, cdsInset3 = 15, utr3Inset5 = 6 )
zeroInsets    <- list( utr5Inset3 = 0, cdsInset5 =  0, cdsInset3 =  0, utr3Inset5 = 0 )

trxRegionCountSizes <- function(insets, trx, cds) {
    sizes <- list( trx = sum(width(trx)) )
    
    if (length(cds) == 1) {
        cdsQStart <- start(cds) + insets$cdsInset5
        cdsQEnd <- end(cds) - insets$cdsInset3
        
        sizes$cds <- if (cdsQEnd < cdsQStart) { NA } else { 1 + cdsQEnd - cdsQStart }
        
        utr5QEnd <- start(cds) - (insets$utr5Inset3 + 1)
        sizes$utr5 <- if (utr5QEnd < 0) { NA } else { 1 + utr5QEnd }
        
        utr3QStart <- end(cds) + (insets$utr3Inset5 + 1)
        sizes$utr3 <- if (utr3QStart >= sum(width(trx))) { NA } else { 1 + sum(width(trx)) - utr3QStart }
    } else {
        sizes$cds <- NA
        sizes$utr5 <- NA
        sizes$utr3 <- NA
    }

    sizes
}

trxRegionCountAligns <- function(asiteOffsets, insets, bamfile, trx, cds) {
    genomicAsites <- getAlignASites(asiteOffsets, bamfile, trx)

    if (length(genomicAsites) > 0) {
        asites <- relativeWithin(start(genomicAsites), trx)
    
        counts <- list( trx = sum(!is.na(asites)) )
        
        if ( length(cds) == 1 ) {
            cdsQStart <- start(cds) + insets$cdsInset5
            cdsQEnd <- end(cds) - insets$cdsInset3
            
            counts$cds <- if (cdsQEnd < cdsQStart) { NA } else { sum(asites >= cdsQStart & asites <= cdsQEnd, na.rm=TRUE) }
            
            utr5QEnd <- start(cds) - (insets$utr5Inset3 + 1)
            counts$utr5 <- if (utr5QEnd < 0) { NA } else { sum(asites <= utr5QEnd, na.rm=TRUE) }
            
            utr3QStart <- end(cds) + (insets$utr3Inset5 + 1)
            counts$utr3 <- if (utr3QStart >= sum(width(trx))) { NA } else { sum(asites >= utr3QStart, na.rm=TRUE) }
        } else {
            counts$cds <- NA
            counts$utr5 <- NA
            counts$utr3 <- NA           
        }
    } else {
        counts <- list( trx = 0 )

        if ( length(cds) == 1) {
            counts$cds <- 0
            counts$utr5 <- 0
            counts$utr3 <- 0
        } else {
            counts$cds <- NA
            counts$utr5 <- NA
            counts$utr3 <- NA
        }
    }
    
    counts
}

countAligns <- function(asiteOffsets, insets, bamfile, trxBed) {
    ttl <- length(trxBed)
    countOne <- function(i) {
        if (i %% 100 == 0) { print(c(i, ttl, as.character(trxBed$name[i]))) }
        trxGR <- transcriptGRanges(trxBed[i])
        cdsIR <- transcriptCdsIRanges(trxGR, start(trxBed$thick)[[i]], end(trxBed$thick)[[i]])        
        trxRegionCountAligns(asiteOffsets, insets, bamfile, trxGR, cdsIR)
    }
    counts <- mclapply(seq(1,ttl), countOne)
    names(counts) <- trxBed$name
    counts
}

countSizes <- function(insets, trxBed) {
    countOne <- function(i) {
        trxGR <- transcriptGRanges(trxBed[i])
        cdsIR <- transcriptCdsIRanges(trxGR, start(trxBed$thick)[[i]], end(trxBed$thick)[[i]])        
        trxRegionCountSizes(insets, trxGR, cdsIR)
    }    
    sizes <- mclapply(seq(1,length(trxBed)), countOne)
    names(sizes) <- trxBed$name
    sizes
}

regionCountFrame <- function(counts) {
    data.frame(trx  = unlist(lapply(counts, function(r) { r$trx })),
               cds  = unlist(lapply(counts, function(r) { r$cds })),
               utr5 = unlist(lapply(counts, function(r) { r$utr5 })),
               utr3 = unlist(lapply(counts, function(r) { r$utr3 })),
               row.names = names(counts))
}

