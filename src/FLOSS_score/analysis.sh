7_#!/bin/bash
#
### variables SGE
### shell du job
#$ -S /bin/bash
### nom du job
#$ -N FLOSS_U937_all_merge
### file d'attente:
#$ -q M6142deb*
### parallel environment & nb cpu (NSLOTS)
#$ -pe openmp32 32
### charger l'environnement utilisateur pour SGE
#$ -cwd
### exporte les variables d'environnement sur les noeuds d'exécution
#$ -V
### mails en debut et fin d'execution
#$ -m be

# init env (should be in ~/.profile)
#source /usr/share/lmod/lmod/init/bash
#source ~/.bashrc

### configurer l'environnement

#cd ${SGE_O_WORKIR}
# nombre de processeurs
#CPU=${NSLOTS}


### Merge bam files from NY5 and hg38 alignements

#samtools view -H /Xnfs/lbmcdb/Ricci_team/HIV_project/results/20180326_RP_U937CHX/Results/03_hisat2_hg38/U937_CHX_all_merged_hg38_sorted.mapped.bam > tmp.sam
#samtools view -H /Xnfs/lbmcdb/Ricci_team/HIV_project/results/20180326_RP_U937CHX/Results/06_hisat2_NY5/U937_CHX_all.bam >> tmp.sam
#samtools view /Xnfs/lbmcdb/Ricci_team/HIV_project/results/20180326_RP_U937CHX/Results/03_hisat2_hg38/U937_CHX_all_merged_hg38_sorted.mapped.bam >> tmp.sam
#samtools view /Xnfs/lbmcdb/Ricci_team/HIV_project/results/20180326_RP_U937CHX/Results/06_hisat2_NY5/U937_CHX_all.bam >> tmp.sam

#samtools view -Sb tmp.sam > /Xnfs/lbmcdb/Ricci_team/HIV_project/results/20180326_RP_U937CHX/Results/10_FLOSS_score/U937_CHX_all_hg38_NY5.bam
#samtools sort -o /Xnfs/lbmcdb/Ricci_team/HIV_project/results/20180326_RP_U937CHX/Results/10_FLOSS_score/U937_CHX_all_hg38_NY5_sort.bam /Xnfs/lbmcdb/Ricci_team/HIV_project/results/20180326_RP_U937CHX/Results/10_FLOSS_score/U937_CHX_all_hg38_NY5.bam
#samtools index /Xnfs/lbmcdb/Ricci_team/HIV_project/results/20180326_RP_U937CHX/Results/10_FLOSS_score/U937_CHX_all_hg38_NY5_sort.bam

#rm tmp.sam /Xnfs/lbmcdb/Ricci_team/HIV_project/results/20180326_RP_U937CHX/Results/10_FLOSS_score/U937_CHX_all_hg38_NY5.bam

### Calculating FLOSS scores
R --no-save < ldist-calc-template.R
# FLOSS scores needed to pick lincs in ldist plots
#R --no-save < floss-calc-mesc.R ####### ---> this script is now merged in the first one
R --no-save < ldist-plots-template.R
R --no-save < floss-plots-template.R
