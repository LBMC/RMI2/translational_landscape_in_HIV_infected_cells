#!/bin/bash
set -e 
nextflow src/workflow_RiboCode.nf -resume \
				-profile psmn \
				--fasta "data/genome/GRCh38.p12.genome_NL43.fa" \
				--gtf "data/annotation/gencode.v28.annotation_NL4.3_forRiboCode_updated.gtf" \
				--output "results/test_ribocode" \
				--fastq "data/fastq/RP_primaryCD4/Ribo-seq-CD4-Donor2-HIV-rep2.fastq.gz"





#				--fastq "data/fastq/{RP_SupT1/RP_SupT1_HIV1_24h_CHX_rep6,RP_SupT1_RNAseI/RP_SupT1_HIV1_24h_CHX_rep6_RNAseI,RP_primaryCD4/Ribo-seq-CD4-Donor2-HIV-rep2}.fastq.gz"
