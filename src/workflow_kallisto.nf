nextflow.enable.dsl=2

/*
./nextflow src/solution_RNASeq.nf --fastq "data/tiny_dataset/fastq/tiny2_R{1,2}.fastq.gz" --fasta "data/tiny_dataset/fasta/tiny_v2_10.fasta" --bed "data/tiny_dataset/annot/tiny.bed" -profile docker
*/

params.fastq = "data/fastq/*{_R1,_R2}.fastq.gz"
params.output = "results"
params.fasta = "data/transcriptome/*.fa"



log.info "input raw : ${params.fastq}"
log.info "outut directory : ${params.output}"
log.info "transcriptome fasta file : ${params.fasta}"


// log.info "adaptor sequence : ${params.adaptorR1}"
// log.info "strand of the library : ${params.strand}"
// log.info ""
// log.info "do fastqc ? : ${params.do_fastqc}"
// log.info "do deduplication ? : ${params.do_dedup}"
// log.info "do post genome alignement ? : ${params.do_postgenome}"
// log.info ""

Channel
  .fromFilePairs( params.fastq )
  .ifEmpty { error "Cannot find any fastq files matching: ${params.fastq}" }
  .set { FASTQ_FILES }
Channel
  .fromPath( params.fasta )
  .ifEmpty { error "Cannot find any index files matching: ${params.fasta}" }
  .set { FASTA_FILE }



include { index_fasta as index } from './nf_modules/kallisto'
include { mapping_fastq_pairedend as mapping } from './nf_modules/kallisto'

workflow {
   index(FASTA_FILE)
   mapping(index.out.index.collect(), FASTQ_FILES, params.output)
}
