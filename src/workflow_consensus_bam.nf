nextflow.enable.dsl=2

params.bam = "data/*.bam"
params.output = "results"
params.fasta = "data/genome/*.fa"



log.info "input alignement : ${params.bam}"
log.info "outut directory : ${params.output}"
log.info "genome fasta file : ${params.fasta}"

Channel
  .fromPath( params.bam )
  .ifEmpty { error "Cannot find any fastq files matching: ${params.bam}" }
  .map { it -> [(it.baseName =~ /([^\.]*)/)[0][1], it]}
  .set { BAM_FILES }
Channel
  .fromPath( params.fasta )
  .ifEmpty { error "Cannot find any index files matching: ${params.fasta}" }
  .set { FASTA_FILE }



include { get_consensus_from_bam as cons } from './nf_modules/get_consensus_seq'

workflow {
   cons(BAM_FILES, FASTA_FILE, params.output)
}
