#!/bin/bash
set -e

nextflow src/workflow_kallisto.nf -resume -profile psmn --fastq "data/fastq/RNAseq_SupT1/*{_R1,_R2}.fastq.gz" --fasta "data/transcriptome/homo_sapiens_cDNA_NL4.3.fa" --output "results/RNAseq_SupT1/kallisto"

nextflow src/workflow_kallisto.nf -resume -profile psmn --fastq "data/fastq/RNAseq_U937/*{_R1,_R2}.fastq.gz" --fasta "data/transcriptome/homo_sapiens_cDNA_NY5.fa" --output "results/RNAseq_U937/kallisto"

nextflow src/workflow_kallisto.nf -resume -profile psmn --fastq "data/fastq/simulated_reads/*{_R1,_R2}.fastq.gz" --fasta "data/transcriptome/homo_sapiens_cDNA_NL4.3.fa" --output "results/simulated_reads/kallisto"
