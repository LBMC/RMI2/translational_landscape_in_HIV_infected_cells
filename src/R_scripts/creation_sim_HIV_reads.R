# Simulation of HIV-1 RNA-seq reads

library( 'Rsubread' )


# Scan through the fasta file to get transcript names and lengths
transcripts <- scanFasta("data/NL43_transcriptome_from_gtf.fa")
nsequences <- nrow(transcripts) - sum(transcripts$Duplicate)

# Assign a random TPM value to each non-duplicated transcript sequence
TPMs <- rep(0, nrow(transcripts))
TPMs[!transcripts$Duplicate] <- rexp(nsequences)

# Generate actual reads.
# The output read file is my-simulated-sample_R1.fastq.gz 
# The true read counts are returned.
true.counts <- simReads(transcript.file = "data/NL43_transcriptome_from_gtf.fa", 
                        expression.levels =  TPMs, 
                        output.prefix =  "simulated_NL4.3",
                        library.size = 1000000,
                        read.length = 100,
                        truth.in.read.names = FALSE,
                        paired.end = TRUE,
                        fragment.length.min = 100, 
                        fragment.length.mean = 125, 
                        fragment.length.max = 150,
                        simplify.transcript.names = T)

print(true.counts[1:10,])
write.csv(x = true.counts, file = "data/fastq/simulated_reads/true_counts_sim_reads.csv")