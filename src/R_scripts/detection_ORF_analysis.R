## Analysis of detection ORF 
library(tidyverse)

filter_ORF_ribocode <- function(df) {
  df <- df %>% filter(gene_id == "NL4.3_R-U5-CDS-U3") %>% 
    select(start_codon, ORF_length, ORF_gstart, ORF_gstop, pval_combined) %>%
    group_by(start_codon, ORF_length, ORF_gstart, ORF_gstop) %>%
    summarise(start_codon = start_codon, ORF_length = ORF_length, ORF_gstart = ORF_gstart, ORF_gstop = ORF_gstop, pval_combined = min(pval_combined))
}

filter_ORF_ribotricer <- function(df) {
  df <- df %>% filter(gene_id == "NL4.3_R-U5-CDS-U3") %>%# 
    mutate(ORF_ID = str_replace(ORF_ID, paste0(transcript_id,"_"), ""))
  tmp <- str_split_fixed(df$ORF_ID, "_", n = 3)
  df$ORF_gstart = tmp[,1]
  df$ORF_gstop = tmp[,2]
  df$ORF_length = tmp[,3]
  df <- df %>%  select(start_codon, ORF_length, ORF_gstart, ORF_gstop) %>%
                uniquesta
}

filter_ORF_ribotish <- function(df) {
  df <- df %>% filter(Gid == "NL4.3_R-U5-CDS-U3")
  tmp <- str_split_fixed(df$GenomePos, ":", n = 3)
  tmp <- str_split_fixed(tmp[,2], "-", n = 2)
  df$ORF_gstart <- tmp[,1]
  df$ORF_gstop <- tmp[,2]
  df$ORF_length <- df$AALen * 3
  df <- df %>% select(StartCodon, ORF_length, ORF_gstart, ORF_gstop, FrameQvalue) %>%
    group_by(ORF_length, ORF_gstart, ORF_gstop) %>%
    summarise(start_codon = StartCodon, ORF_length = ORF_length, ORF_gstart = ORF_gstart, ORF_gstop = ORF_gstop, pval_combined = min(FrameQvalue)) %>%
    unique
}
# Ribocode ----------------------------------------------------------------

df <- read.csv(file = "results/detection_orfs/ribocode/ORF_dectection/Ribo-seq-CD4-Donor1-HIV-rep2_collapsed.txt", sep = "\t")
primaryCD4_donor1 <- filter_ORF_ribocode(df)

df <- read.csv(file = "results/detection_orfs/ribocode/ORF_dectection/Ribo-seq-CD4-Donor2-HIV-rep2_collapsed.txt", sep = "\t")
primaryCD4_donor2 <- filter_ORF_ribocode(df)

df <- read.csv(file = "results/detection_orfs/ribocode/ORF_dectection/RP_Jurkat_all_CHX_collapsed.txt", sep = "\t")
JK_CHX_all <- filter_ORF_ribocode(df)

df <- read.csv(file = "results/detection_orfs/ribocode/ORF_dectection/RP_SupT1_all_CHX_RNAseI_collapsed.txt", sep = "\t")
SupT1_RNAseI_all <- filter_ORF_ribocode(df)

df <- read.csv(file = "results/detection_orfs/ribocode/ORF_dectection/RP_U937_all_CHX_collapsed.txt", sep = "\t")
U937_all <- filter_ORF_ribocode(df)

df <- read.csv(file = "results/detection_orfs/ribocode/ORF_dectection/RP_U937_24h_1MKCl_CHX_rep1_collapsed.txt", sep = "\t")
U937_1MKCl_rep1 <- filter_ORF_ribocode(df)

df <- read.csv(file = "results/detection_orfs/ribocode/ORF_dectection/RP_U937_24h_1MKCl_CHX_rep2_collapsed.txt", sep = "\t")
U937_1MKCl_rep2 <- filter_ORF_ribocode(df)

aggregate_Ribocode <- rbind(primaryCD4_donor1,
                            primaryCD4_donor2,
                            JK_CHX_all,
                            SupT1_RNAseI_all,
                            U937_all,
                            U937_1MKCl_rep1,
                            U937_1MKCl_rep2) %>%
                      group_by(start_codon, ORF_gstart) %>%
                      summarise(start_codon = start_codon, ORF_gstart = ORF_gstart, pval_combined = min(pval_combined)) %>%
                      unique

# Ribotricer --------------------------------------------------------------
df <- read.csv(file = "results/detection_orfs/ribotricer/detect_orfs/Ribo-seq-CD4-Donor1-HIV-rep2_translating_ORFs.tsv", sep = "\t")
primaryCD4_donor1 <- filter_ORF_ribotricer(df)

df <- read.csv(file = "results/detection_orfs/ribotricer/detect_orfs/Ribo-seq-CD4-Donor2-HIV-rep2_translating_ORFs.tsv", sep = "\t")
primaryCD4_donor2 <- filter_ORF_ribotricer(df)

df <- read.csv(file = "results/detection_orfs/ribotricer/detect_orfs/RP_Jurkat_all_CHX_translating_ORFs.tsv", sep = "\t")
JK_CHX_all <- filter_ORF_ribotricer(df)

# df <- read.csv(file = "results/detection_orfs/ribotricer/detect_orfs/RP_SupT1_all_CHX_RNAseI_translating_ORFs.tsv", sep = "\t")
# SupT1_RNAseI_all <- filter_ORF_ribotricer(df)

df <- read.csv(file = "results/detection_orfs/ribotricer/detect_orfs/RP_U937_all_CHX_translating_ORFs.tsv", sep = "\t")
U937_all <- filter_ORF_ribotricer(df)

df <- read.csv(file = "results/detection_orfs/ribotricer/detect_orfs/RP_U937_24h_1MKCl_CHX_rep1_translating_ORFs.tsv", sep = "\t")
U937_1MKCl_rep1 <- filter_ORF_ribotricer(df)

df <- read.csv(file = "results/detection_orfs/ribotricer/detect_orfs/RP_U937_24h_1MKCl_CHX_rep2_translating_ORFs.tsv", sep = "\t")
U937_1MKCl_rep2 <- filter_ORF_ribotricer(df)

aggregate_Ribotricer <- rbind(primaryCD4_donor1,
                            primaryCD4_donor2,
                            JK_CHX_all,
                            # SupT1_RNAseI_all,
                            U937_all,
                            U937_1MKCl_rep1,
                            U937_1MKCl_rep2) %>%
  group_by(start_codon, ORF_gstart) %>%
  summarise(start_codon = start_codon, ORF_gstart = ORF_gstart) %>%
  unique

# ribotish ----------------------------------------------------------------

# df <- read.csv(file = "results/detection_orfs/ribotricer/detect_orfs/Ribo-seq-CD4-Donor1-HIV-rep2_translating_ORFs.tsv", sep = "\t")
# primaryCD4_donor1 <- filter_ORF_ribotish(df)

# df <- read.csv(file = "results/detection_orfs/ribotricer/detect_orfs/Ribo-seq-CD4-Donor2-HIV-rep2_translating_ORFs.tsv", sep = "\t")
# primaryCD4_donor2 <- filter_ORF_ribotish(df)

df <- read.csv(file = "results/detection_orfs/ribotish/prediction/RP_Jurkat_all_CHX_pred_all.txt", sep = "\t")
JK_CHX_all <- filter_ORF_ribotish(df)

df <- read.csv(file = "results/detection_orfs/ribotish/prediction/RP_SupT1_all_CHX_RNAseI_pred_all.txt", sep = "\t")
SupT1_RNAseI_all <- filter_ORF_ribotish(df)

df <- read.csv(file = "results/detection_orfs/ribotish/prediction/RP_U937_all_CHX_pred_all.txt", sep = "\t")
U937_all <- filter_ORF_ribotish(df)

df <- read.csv(file = "results/detection_orfs/ribotish/prediction/RP_U937_24h_1MKCl_CHX_rep1_pred_all.txt", sep = "\t")
U937_1MKCl_rep1 <- filter_ORF_ribotish(df)

df <- read.csv(file = "results/detection_orfs/ribotish/prediction/RP_U937_24h_1MKCl_CHX_rep2_pred_all.txt", sep = "\t")
U937_1MKCl_rep2 <- filter_ORF_ribotish(df)

df <- read.csv(file = "results/detection_orfs/ribotish/prediction/RP_Jurkat_merged_HAR_pred_all.txt", sep = "\t")
JK_HAR_all <- filter_ORF_ribotish(df)

df <- read.csv(file = "results/detection_orfs/ribotish/prediction/RP_U937_merged_HAR_pred_all.txt", sep = "\t")
U937_HAR_all <- filter_ORF_ribotish(df)

aggregate_Ribotrish <- rbind(JK_HAR_all,
                              # primaryCD4_donor1,
                              # primaryCD4_donor2,
                              JK_CHX_all,
                              SupT1_RNAseI_all,
                              U937_all,
                              U937_HAR_all,
                              U937_1MKCl_rep1,
                              U937_1MKCl_rep2) %>%
  group_by(start_codon, ORF_gstart) %>%
  summarise(start_codon = start_codon, ORF_gstart = ORF_gstart) %>%
  unique %>%
  arrange(as.numeric(ORF_gstart))


# VennDiagram -------------------------------------------------------------

data_list <- list(ribocode = paste(unlist(aggregate_Ribocode[,1]), unlist(aggregate_Ribocode[,2]), sep = "_"),
                  ribotricer = paste(unlist(aggregate_Ribotricer[,1]), unlist(aggregate_Ribotricer[,2]), sep = "_"),
                  ribotish = paste(unlist(aggregate_Ribotrish[,1]), unlist(aggregate_Ribotrish[,2]), sep = "_"))

library(ggvenn)
ggvenn(data_list)

canonicals <- c("ATG_336", # Gag
                "ATG_4581", # Vif
                "ATG_5099", # Vpr
                "ATG_5370", # Tat
                "ATG_5509", # Rev
                "ATG_5601", # Vpu
                "ATG_5836", # Env
                "ATG_8327") # Nef

data_canonicals <- list(ribocode = data_list[[1]][data_list[[1]] %in% canonicals],
                        ribotricer = data_list[[2]][data_list[[2]] %in% canonicals],
                        ribotish =  data_list[[3]][data_list[[3]] %in% canonicals])
ggvenn(data_canonicals) + ggtitle("Canonicals ATG")


# RiboCode analysis  ------------------------------------------------------

df <- read.csv(file = "results/detection_orfs/ribocode/ORF_dectection/Ribo-seq-CD4-Donor1-HIV-rep2_collapsed.txt", sep = "\t")
primaryCD4_donor1 <- filter_ORF_ribocode(df)
primaryCD4_donor1$sample = "primaryCD4_donor"

df <- read.csv(file = "results/detection_orfs/ribocode/ORF_dectection/Ribo-seq-CD4-Donor2-HIV-rep2_collapsed.txt", sep = "\t")
primaryCD4_donor2 <- filter_ORF_ribocode(df)
primaryCD4_donor2$sample = "primaryCD4_donor"

df <- read.csv(file = "results/detection_orfs/ribocode/ORF_dectection/RP_Jurkat_all_CHX_collapsed.txt", sep = "\t")
JK_CHX_all <- filter_ORF_ribocode(df)
JK_CHX_all$sample = "JK_CHX_all"

df <- read.csv(file = "results/detection_orfs/ribocode/ORF_dectection/RP_SupT1_all_CHX_RNAseI_collapsed.txt", sep = "\t")
SupT1_RNAseI_all <- filter_ORF_ribocode(df)
SupT1_RNAseI_all$sample = "SupT1_RNAseI_all"

df <- read.csv(file = "results/detection_orfs/ribocode/ORF_dectection/RP_U937_all_CHX_collapsed.txt", sep = "\t")
U937_all <- filter_ORF_ribocode(df)
U937_all$sample = "U937_all"

df <- read.csv(file = "results/detection_orfs/ribocode/ORF_dectection/RP_U937_24h_1MKCl_CHX_rep1_collapsed.txt", sep = "\t")
U937_1MKCl_rep1 <- filter_ORF_ribocode(df)
U937_1MKCl_rep1$sample = "U937_1MKCl"

df <- read.csv(file = "results/detection_orfs/ribocode/ORF_dectection/RP_U937_24h_1MKCl_CHX_rep2_collapsed.txt", sep = "\t")
U937_1MKCl_rep2 <- filter_ORF_ribocode(df)
U937_1MKCl_rep2$sample = "U937_1MKCl"

aggregate_Ribocode <- rbind(primaryCD4_donor1,
                            primaryCD4_donor2,
                            JK_CHX_all,
                            SupT1_RNAseI_all,
                            U937_all,
                            U937_1MKCl_rep1,
                            U937_1MKCl_rep2) %>%
  group_by(start_codon, ORF_gstart, sample) %>%
  summarise(start_codon = start_codon, ORF_gstart = ORF_gstart, pval_combined = min(pval_combined), sample = sample) %>%
  unique

ggplot(data = aggregate_Ribocode, aes(x = sample)) + 
  geom_bar() + 
  theme_bw()

canonicals <- c(336, # Gag
                4581, # Vif
                5099, # Vpr
                5370, # Tat
                5509, # Rev
                5601, # Vpu
                5836, # Env
                8327) # Nef

ggplot(data = aggregate_Ribocode, aes(x = sample, fill = ORF_gstart %in% canonicals)) + 
  geom_bar(position = position_dodge()) +
  theme_bw()

write_csv(aggregate_Ribocode, file = "results/detection_orfs/ribocode/ORF_dectection/compil_RiboCode_ORFs_HIV1.csv")
