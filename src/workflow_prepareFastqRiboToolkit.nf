nextflow.enable.dsl=2

params.fastq = "data/fastq/*.fastq.gz"
params.output = "results"

log.info "input raw : ${params.fastq}"
log.info "outut directory : ${params.output}"

Channel
  .fromPath( params.fastq )
  .ifEmpty { error "Cannot find any fastq files matching: ${params.fastq}" }
  .map { it -> [(it.baseName =~ /([^\.]*)/)[0][1], it]}
  .set { FASTQ_FILES }

include { fastp_singleend } from './nf_modules/fastp'
include { fq2collapedfa } from './nf_modules/fq2collapedFa'

workflow {
   // trimming and QC analysis --------------------------------------------------------------------------
       fastp_singleend(FASTQ_FILES, params.output)
       fq2collapedfa(fastp_singleend.out.FASTQ, params.output)
}
