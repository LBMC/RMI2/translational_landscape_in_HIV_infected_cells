#!/bin/bash

nextflow src/workflow_plastid.nf -profile psmn -resume --gtf data/annotation/Homo_sapiens.GRCh38.84.gtf --bam results/RP_primaryCD4/02_mapping_genome/*.bam

nextflow src/workflow_plastid.nf -profile psmn -resume --gtf data/annotation/Homo_sapiens.GRCh38.84.gtf --bam results/RP_SupT1/02_mapping_genome_dedup/RP_SupT1_HIV1_24h_CHX_repAll_dedup.bam

nextflow src/workflow_plastid.nf -profile psmn -resume --gtf data/annotation/Homo_sapiens.GRCh38.84.gtf --bam results/RP_SupT1_RNAseI/02_mapping_genome_dedup/RP_SupT1_HIV1_24h_CHX_repAll_RNAseI_dedup.bam

nextflow src/workflow_plastid.nf -profile psmn -resume --gtf data/annotation/Homo_sapiens.GRCh38.84.gtf --bam results/RP_U937/02_mapping_genome_dedup/RP_U937_all_CHX_dedup.bam

nextflow src/workflow_plastid.nf -profile psmn -resume --gtf data/annotation/Homo_sapiens.GRCh38.84.gtf --bam results/RP_U937_HAR/02_mapping_genome_dedup/RP_U937_merged_HAR_dedup.bam
