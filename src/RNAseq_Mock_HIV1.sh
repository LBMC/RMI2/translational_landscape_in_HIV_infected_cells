#!/usr/bin/env bash

set -e

nextflow src/workflow_RNAseq.nf -profile psmn\
                                         -resume \
                                         --fastq "data/fastq/RNAseq_SupT1/*{_R1,_R2}.fastq.gz"\
                                         --output "results/RNAseq_SupT1"\
                                          --filter "data/filter/human_rRNA_tRNA_snRNA*.bt2"\
                                          --index_genome "data/genome/*.ht2"\
                                          --index_postgenome "data/post_genome/NL4.3/*.ht2"\
                                          --gtf "data/annotation/Homo_sapiens.GRCh38.84.gtf"\
                                          --gtf_postgenome "data/annotation/pNL4.3_CDS.gff"

nextflow src/workflow_RNAseq.nf -profile psmn \
                                            -resume \
                                            --fastq "data/fastq/RNAseq_U937/*{_R1,_R2}.fastq.gz"\
                                            --output "results/RNAseq_U937"\
                                           --filter "data/filter/human_rRNA_tRNA_snRNA*.bt2"\
                                           --index_genome "data/genome/*.ht2"\
                                           --index_postgenome "data/post_genome/M38431.1/*.ht2"\
                                           --gtf "data/annotation/Homo_sapiens.GRCh38.84.gtf"\
                                           --gtf_postgenome "data/annotation/NY5_CDS.gff"

