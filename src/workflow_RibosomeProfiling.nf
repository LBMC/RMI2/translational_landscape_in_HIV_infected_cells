nextflow.enable.dsl=2

/*
./nextflow src/solution_RNASeq.nf --fastq "data/tiny_dataset/fastq/tiny2_R{1,2}.fastq.gz" --fasta "data/tiny_dataset/fasta/tiny_v2_10.fasta" --bed "data/tiny_dataset/annot/tiny.bed" -profile docker
*/

params.fastq = "data/fastq/*.fastq.gz"
params.output = "results"
params.filter = "data/filter/human_rRNA_tRNA/*.bt2"
params.index_genome = "data/genome/*.ht2"
params.gtf = "data/annotation/*.gtf"
params.gtf_postgenome = "data/annotation/*.gtf"
params.index_postgenome = "data/post_genome/*.ht2"
params.do_dedup = false


log.info ""
log.info "---------------------------------------------------------"
log.info "                     PARAMETERS                          "
log.info "---------------------------------------------------------"
log.info "input raw : \t\t${params.fastq}"
log.info "outut directory : \t${params.output}"
log.info "filter index files : \t${params.filter}"
log.info "genome index : \t\t${params.index_genome}"
log.info "gtf file : \t\t${params.gtf}"
log.info "post-genome index : \t${params.index_postgenome}"
log.info ""
log.info "do deduplication ? : ${params.do_dedup}"
log.info ""
log.info "---------------------------------------------------------"
log.info ""

Channel
   .fromPath ( params.index_genome )
   .ifEmpty { error "Cannot find any index files matching: ${params.index_genome}" }
   .set { GENOME_INDEX }
Channel
  .fromPath( params.fastq )
  .ifEmpty { error "Cannot find any fastq files matching: ${params.fastq}" }
  .map { it -> [(it.baseName =~ /([^\.]*)/)[0][1], it]}
  .set { FASTQ_FILES }
Channel
  .fromPath( params.filter )
  .ifEmpty { error "Cannot find any index files matching: ${params.filter}" }
  .set { FILTER_INDEX }
Channel
  .fromPath ( params.index_postgenome )
  .ifEmpty { error "Cannot find any index files matching: ${params.index_postgenome}" }
  .set { POSTGENOME_INDEX }
Channel
 .fromPath( params.gtf )
 .ifEmpty { error "Cannot find any gtf file matching: ${params.gtf}" }
 .set { GTF_FILE }
 Channel
  .fromPath( params.gtf_postgenome )
  .ifEmpty { error "Cannot find any gtf file matching: ${params.gtf_postgenome}" }
  .set { GTFPOSTGENOME_FILE }


include { fastp_singleend } from './nf_modules/fastp'
include { bowtie_singleend } from './nf_modules/bowtie2'
include { mapping_single as mapping_genome } from './nf_modules/hisat2'
include { mapping_single2 as mapping_postgenome } from './nf_modules/hisat2'
include { dedup ; dedup_postgenome} from './nf_modules/deduplication'
include { counting_exon ; counting_CDS } from './nf_modules/htseq'
include { counting_exon as counting_exon_postgenome ; counting_CDS as counting_CDS_postgenome } from './nf_modules/htseq'
include { merging_counts ; merging_counts_CDS} from './nf_modules/htseq'
include {calc_scalingFactor_with_postgenome ; coverage_with_postgenome} from './nf_modules/bam_to_bigwig'

workflow {
// trimming and QC analysis --------------------------------------------------------------------------
    fastp_singleend(FASTQ_FILES, params.output)

// Filtering -----------------------------------------------------------------------------------------
    bowtie_singleend(fastp_singleend.out.FASTQ, FILTER_INDEX.collect(), params.output)

// Mapping on genome and post genome -----------------------------------------------------------------
    mapping_genome(bowtie_singleend.out.FASTQ, GENOME_INDEX.collect(), params.output)
    mapping_postgenome(mapping_genome.out.FASTQ, POSTGENOME_INDEX.collect(), params.output)

// deduplication of reads ----------------------------------------------------------------------------
    if (params.do_dedup) {
      dedup(mapping_genome.out.BAM, params.output)
      dedup_postgenome(mapping_postgenome.out.BAM, params.output)
      // Counting with HTseq -------------------------------------------------------------------------------
      counting_exon(dedup.out.BAM, GTF_FILE.collect(), params.output)
      counting_CDS(dedup.out.BAM, GTF_FILE.collect(), params.output)
      counting_exon_postgenome(dedup_postgenome.out.BAM, GTFPOSTGENOME_FILE.collect(), params.output)
      counting_CDS_postgenome(dedup_postgenome.out.BAM, GTFPOSTGENOME_FILE.collect(), params.output)
    } else {
      // Counting with HTseq -------------------------------------------------------------------------------
      counting_exon(mapping_genome.out.BAM, GTF_FILE.collect(), params.output)
      counting_CDS(mapping_genome.out.BAM, GTF_FILE.collect(), params.output)
      counting_exon_postgenome(mapping_postgenome.out.BAM, GTFPOSTGENOME_FILE.collect(), params.output)
      counting_CDS_postgenome(mapping_postgenome.out.BAM, GTFPOSTGENOME_FILE.collect(), params.output)
    }

// Merging exon counts -------------------------------------------------------------------------------
    counting_exon.out.COUNT.join(counting_exon_postgenome.out.COUNT).set{counting_exon_merged}
    merging_counts(counting_exon_merged, params.output)

// Merging CDS counts --------------------------------------------------------------------------------
    counting_CDS.out.COUNT.join(counting_CDS_postgenome.out.COUNT).set{counting_CDS_merged}
    merging_counts_CDS(counting_exon_merged, params.output)

// Bam to bigwig -------------------------------------------------------------------------------------
    if(params.do_dedup){
	     dedup.out.BAM.join(dedup.out.BAI)
                     .join(dedup_postgenome.out.BAM)
                     .join(dedup_postgenome.out.BAI)
                     .set{mapping_merged}
    } else {
	     mapping_genome.out.BAM.join(mapping_genome.out.BAI)
                              .join(mapping_postgenome.out.BAM)
                              .join(mapping_postgenome.out.BAI)
                              .set{mapping_merged}
    }

    calc_scalingFactor_with_postgenome(mapping_merged)
    calc_scalingFactor_with_postgenome.out.FACTOR.join(mapping_merged)
                             .set{factor_and_bam_merged}
    coverage_with_postgenome(factor_and_bam_merged, params.output)
}
