#!/usr/bin/env bash

set -e

nextflow src/workflow_RibosomeProfiling.nf -profile psmn \
                                         -resume \
                                         --fastq "data/fastq/RP_U937/*.fastq.gz"\
                                         --output "results/RP_U937"\
                                          --filter "data/filter/human_rRNA_tRNA_snRNA*.bt2"\
                                          --index_genome "data/genome/*.ht2"\
                                          --index_postgenome "data/post_genome/M38431.1/*.ht2"\
                                          --gtf "data/annotation/Homo_sapiens.GRCh38.84.gtf"\
                                          --gtf_postgenome "data/annotation/NY5_CDS.gff"

nextflow src/workflow_RibosomeProfiling.nf -profile psmn \
                                         -resume \
                                         --fastq "data/fastq/RP_U937_Jurkat_HAR/*U937*.fastq.gz"\
                                         --output "results/RP_U937_HAR"\
                                          --filter "data/filter/human_rRNA_tRNA_snRNA*.bt2"\
                                          --index_genome "data/genome/*.ht2"\
                                          --index_postgenome "data/post_genome/M38431.1/*.ht2"\
                                          --gtf "data/annotation/Homo_sapiens.GRCh38.84.gtf"\
                                          --gtf_postgenome "data/annotation/NY5_CDS.gff"

nextflow src/workflow_RibosomeProfiling.nf -profile psmn \
                                            -resume \
                                            --fastq "data/fastq/RP_SupT1/*.fastq.gz"\
                                            --output "results/RP_SupT1"\
                                           --filter "data/filter/human_rRNA_tRNA_snRNA*.bt2"\
                                           --index_genome "data/genome/*.ht2"\
                                           --index_postgenome "data/post_genome/NL4.3/*.ht2"\
                                           --gtf "data/annotation/Homo_sapiens.GRCh38.84.gtf"\
                                           --gtf_postgenome "data/annotation/pNL4.3_CDS.gff"

nextflow src/workflow_RibosomeProfiling.nf -profile psmn \
                                            -resume \
                                            --fastq "data/fastq/RP_SupT1_Harr/*{Mock,HIV1}*.fastq.gz"\
                                            --output "results/RP_SupT1_Harr"\
                                           --filter "data/filter/human_rRNA_tRNA_snRNA*.bt2"\
                                           --index_genome "data/genome/*.ht2"\
                                           --index_postgenome "data/post_genome/NL4.3/*.ht2"\
                                           --gtf "data/annotation/Homo_sapiens.GRCh38.84.gtf"\
                                           --gtf_postgenome "data/annotation/pNL4.3_CDS.gff"

nextflow src/workflow_RibosomeProfiling.nf -profile psmn \
                                            -resume \
                                            --fastq "data/fastq/RP_SupT1_RNAseI/*{Mock,HIV1}*.fastq.gz"\
                                            --output "results/RP_SupT1_RNAseI"\
                                           --filter "data/filter/human_rRNA_tRNA_snRNA*.bt2"\
                                           --index_genome "data/genome/*.ht2"\
                                           --index_postgenome "data/post_genome/NL4.3/*.ht2"\
                                           --gtf "data/annotation/Homo_sapiens.GRCh38.84.gtf"\
                                           --gtf_postgenome "data/annotation/pNL4.3_CDS.gff"

nextflow src/workflow_RibosomeProfiling.nf -profile psmn \
                                            -resume \
                                            --fastq "data/fastq/RL_SupT1/*{Mock,HIV1}*.fastq.gz"\
                                            --output "results/RL_SupT1"\
                                           --filter "data/filter/human_rRNA_tRNA_snRNA*.bt2"\
                                           --index_genome "data/genome/*.ht2"\
                                           --index_postgenome "data/post_genome/NL4.3/*.ht2"\
                                           --gtf "data/annotation/Homo_sapiens.GRCh38.84.gtf"\
                                           --gtf_postgenome "data/annotation/pNL4.3_CDS.gff"

nextflow src/workflow_RibosomeProfiling.nf -profile psmn \
                                            -resume \
                                            --fastq "data/fastq/RP_primaryCD4/*.fastq.gz"\
                                            --output "results/RP_primaryCD4"\
                                           --filter "data/filter/human_rRNA_tRNA_snRNA*.bt2"\
                                           --index_genome "data/genome/*.ht2"\
                                           --index_postgenome "data/post_genome/NL4.3/*.ht2"\
                                           --gtf "data/annotation/Homo_sapiens.GRCh38.84.gtf"\
                                           --gtf_postgenome "data/annotation/pNL4.3_CDS.gff"

nextflow src/workflow_RibosomeProfiling.nf -profile psmn \
                                            -resume \
                                            --fastq "data/fastq/RP_IP_HEK/*{Mock,HIV1}*.fastq.gz"\
                                            --output "results/RP_IP_HEK"\
                                           --filter "data/filter/human_rRNA_tRNA_snRNA*.bt2"\
                                           --index_genome "data/genome/*.ht2"\
                                           --index_postgenome "data/post_genome/NL4.3/*.ht2"\
                                           --gtf "data/annotation/Homo_sapiens.GRCh38.84.gtf"\
                                           --gtf_postgenome "data/annotation/pNL4.3_CDS.gff"

nextflow src/workflow_RibosomeProfiling.nf -profile psmn \
                                            -resume \
                                            --fastq "data/fastq/RP_U937_Jurkat_HAR/*Jurkat*.fastq.gz"\
                                            --output "results/RP_Jurkat_HAR"\
                                           --filter "data/filter/human_rRNA_tRNA_snRNA*.bt2"\
                                           --index_genome "data/genome/*.ht2"\
                                           --index_postgenome "data/post_genome/NL4.3/*.ht2"\
                                           --gtf "data/annotation/Homo_sapiens.GRCh38.84.gtf"\
                                           --gtf_postgenome "data/annotation/pNL4.3_CDS.gff"

nextflow src/workflow_RibosomeProfiling.nf -profile psmn \
                                            -resume \
                                            --fastq "data/fastq/RP_Jurkat/*.fastq.gz"\
                                            --output "results/RP_Jurkat"\
                                           --filter "data/filter/human_rRNA_tRNA_snRNA*.bt2"\
                                           --index_genome "data/genome/*.ht2"\
                                           --index_postgenome "data/post_genome/NL4.3/*.ht2"\
                                           --gtf "data/annotation/Homo_sapiens.GRCh38.84.gtf"\
                                           --gtf_postgenome "data/annotation/pNL4.3_CDS.gff"

