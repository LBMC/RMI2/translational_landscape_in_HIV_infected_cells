#!/bin/bash
#set -e

#################################################################################################################
### RIBOCODE ######################## 
nextflow src/workflow_RiboCode.nf -resume \
				-profile psmn \
				--fasta "data/genome/GRCh38.p12.genome_NL43.fa" \
				--gtf "data/annotation/gencode.v28.annotation_NL4.3_forRiboCode_updated.gtf" \
				--output "results/detection_orfs" \
				--fastq "data/fastq/RP_primaryCD4/*.fastq.gz"

nextflow src/workflow_RiboCode.nf -resume \
				-profile psmn \
				--fasta "data/genome/GRCh38.p12.genome_NL43.fa" \
				--gtf "data/annotation/gencode.v28.annotation_NL4.3_forRiboCode_updated.gtf" \
				--output "results/detection_orfs" \
				--fastq "data/fastq/RP_U937/RP_U937_all_CHX.fastq.gz"
nextflow src/workflow_RiboCode.nf -resume \
				-profile psmn \
				--fasta "data/genome/GRCh38.p12.genome_NL43.fa" \
				--gtf "data/annotation/gencode.v28.annotation_NL4.3_forRiboCode_updated.gtf" \
				--output "results/detection_orfs" \
				--fastq "data/fastq/RP_Jurkat/RP_Jurkat_all_CHX.fastq.gz"

nextflow src/workflow_RiboCode.nf -resume \
				-profile psmn \
				--fasta "data/genome/GRCh38.p12.genome_NL43.fa" \
				--gtf "data/annotation/gencode.v28.annotation_NL4.3_forRiboCode_updated.gtf" \
				--output "results/detection_orfs" \
				--fastq "data/fastq/RP_SupT1_RNAseI/*.fastq.gz"
################################################################################################################
### RIBOTISH ########################
nextflow src/workflow_RiboTish.nf -profile psmn -resume \
				--fastq "data/fastq/RP_U937_Jurkat_HAR/*merged_HAR.fastq.gz" \
				--fasta "data/genome/GRCh38.p12.genome_NL43.fa" \
				--output "results/detection_orfs" \
				--gtf "data/annotation/gencode.v28.annotation_NL4.3_forRiboCode_updated.gtf"

nextflow src/workflow_RiboTish_CHX.nf -profile psmn -resume \
				--fastq "data/fastq/RP_Jurkat/RP_Jurkat_all_CHX.fastq.gz" \
				--fasta "data/genome/GRCh38.p12.genome_NL43.fa" \
				--gtf "data/annotation/gencode.v28.annotation_NL4.3_forRiboCode_updated.gtf" \
				--output "results/detection_orfs"

nextflow src/workflow_RiboTish_CHX.nf -profile psmn -resume \
				--fastq "data/fastq/RP_U937/RP_U937_all_CHX.fastq.gz" \
				--fasta "data/genome/GRCh38.p12.genome_NL43.fa" \
				--gtf "data/annotation/gencode.v28.annotation_NL4.3_forRiboCode_updated.gtf" \
				--output "results/detection_orfs"

nextflow src/workflow_RiboTish_CHX.nf -profile psmn -resume \
				--fastq "data/fastq/RP_SupT1_RNAseI/*.fastq.gz" \
				--fasta "data/genome/GRCh38.p12.genome_NL43.fa" \
				--gtf "data/annotation/gencode.v28.annotation_NL4.3_forRiboCode_updated.gtf" \
				--output "results/detection_orfs"

nextflow src/workflow_RiboTish.nf -profile psmn -resume \
				--fastq "data/fastq/RP_SupT1_Harr/*.fastq.gz" \
				--fasta "data/genome/GRCh38.p12.genome_NL43.fa" \
				--output "results/detection_orfs" \
				--gtf "data/annotation/gencode.v28.annotation_NL4.3_forRiboCode_updated.gtf"

###############################################################################################################
### RIBOTRICER ######################

nextflow src/workflow_RiboTricer.nf -resume \
                                -profile psmn \
                                --fasta "data/genome/GRCh38.p12.genome_NL43.fa" \
                                --gtf "data/annotation/gencode.v28.annotation_NL4.3_forRiboCode_updated.gtf" \
                                --output "results/detection_orfs" \
                                --fastq "data/fastq/RP_primaryCD4/*.fastq.gz"

nextflow src/workflow_RiboTricer.nf -resume \
                                -profile psmn \
                                --fasta "data/genome/GRCh38.p12.genome_NL43.fa" \
                                --gtf "data/annotation/gencode.v28.annotation_NL4.3_forRiboCode_updated.gtf" \
                                --output "results/detection_orfs" \
                                --fastq "data/fastq/RP_U937/RP_U937_all_CHX.fastq.gz"

nextflow src/workflow_RiboTricer.nf -resume \
                                -profile psmn \
                                --fasta "data/genome/GRCh38.p12.genome_NL43.fa" \
                                --gtf "data/annotation/gencode.v28.annotation_NL4.3_forRiboCode_updated.gtf" \
                                --output "results/detection_orfs" \
                                --fastq "data/fastq/RP_Jurkat/RP_Jurkat_all_CHX.fastq.gz"
nextflow src/workflow_RiboTricer.nf -resume \
                                -profile psmn \
                                --fasta "data/genome/GRCh38.p12.genome_NL43.fa" \
                                --gtf "data/annotation/gencode.v28.annotation_NL4.3_forRiboCode_updated.gtf" \
                                --output "results/detection_orfs" \
                                --fastq "data/fastq/RP_SupT1_RNAseI/*.fastq.gz"


nextflow src/workflow_RiboCode.nf -resume \
				-profile psmn \
				--fasta "data/genome/GRCh38.p12.genome_NL43.fa" \
				--gtf "data/annotation/gencode.v28.annotation_NL4.3_forRiboCode_updated.gtf" \
				--output "results/detection_orfs" \
				--fastq "data/fastq/RP_U937/RP_U937_24h_1MKCl_CHX_rep*.fastq.gz"
nextflow src/workflow_RiboTish_CHX.nf -resume \
				-profile psmn \
				--fasta "data/genome/GRCh38.p12.genome_NL43.fa" \
				--gtf "data/annotation/gencode.v28.annotation_NL4.3_forRiboCode_updated.gtf" \
				--output "results/detection_orfs" \
				--fastq "data/fastq/RP_U937/RP_U937_24h_1MKCl_CHX_rep*.fastq.gz"
nextflow src/workflow_RiboTricer.nf -resume \
				-profile psmn \
				--fasta "data/genome/GRCh38.p12.genome_NL43.fa" \
				--gtf "data/annotation/gencode.v28.annotation_NL4.3_forRiboCode_updated.gtf" \
				--output "results/detection_orfs" \
				--fastq "data/fastq/RP_U937/RP_U937_24h_1MKCl_CHX_rep*.fastq.gz"
