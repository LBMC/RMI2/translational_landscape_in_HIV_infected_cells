nextflow.enable.dsl=2

params.fastq = "data/fastq/*.fastq.gz"
params.output = "results"
params.filter = "data/filter/*snRNA*.bt2"
params.fasta = "data/genome/*.fa"
params.gtf = "data/annotation/*.gtf"


log.info "input raw : ${params.fastq}"
log.info "outut directory : ${params.output}"
log.info "filter index files : ${params.filter}"
log.info "genome fasta : ${params.fasta}"
log.info "gtf file : ${params.gtf}"
log.info ""

Channel
   .fromPath ( params.fasta )
   .ifEmpty { error "Cannot find any index files matching: ${params.fasta}" }
   .set { GENOME_FASTA }
Channel
  .fromPath( params.fastq )
  .ifEmpty { error "Cannot find any fastq files matching: ${params.fastq}" }
  .map { it -> [(it.baseName =~ /([^\.]*)/)[0][1], it]}
  .set { FASTQ_FILES }
Channel
  .fromPath( params.filter )
  .ifEmpty { error "Cannot find any index files matching: ${params.filter}" }
  .set { FILTER_INDEX }
Channel
 .fromPath( params.gtf )
 .ifEmpty { error "Cannot find any gtf file matching: ${params.gtf}" }
 .set { GTF_FILE }


include { fastp_singleend } from './nf_modules/fastp'
include { bowtie_singleend } from './nf_modules/bowtie2'
include { index_fasta ; mapping_RP } from './nf_modules/star'
include { quality_HAR as quality ; prediction_HAR as prediction } from './nf_modules/ribotish'
include { index_bam ; merge_bam } from './nf_modules/hisat2'

workflow {
// trimming and QC analysis --------------------------------------------------------------------------
    fastp_singleend(FASTQ_FILES, params.output)

// Filtering -----------------------------------------------------------------------------------------
    bowtie_singleend(fastp_singleend.out.FASTQ, FILTER_INDEX.collect(), params.output)

// Index genome
    index_fasta(GENOME_FASTA, GTF_FILE)

// Mapping on genome and post genome -----------------------------------------------------------------
    mapping_RP(bowtie_singleend.out.FASTQ, index_fasta.out.INDEX_FILES.collect(), params.output)
    index_bam(mapping_RP.out.BAM_GENOME)

    mapping_RP.out.BAM_GENOME.join(index_bam.out.INDEX_FILE)
			     .set{BAM_INDEXED}
// RiboTish prepare_transcripts -----------------------------------------------------------------
   quality(GTF_FILE.collect(), BAM_INDEXED, params.output) 

   BAM_INDEXED.join(quality.out.QUALITY_FILE)
	      .set{FILES_FOR_PREDICTION}   

   prediction( GTF_FILE.collect(), GENOME_FASTA.collect(), FILES_FOR_PREDICTION, params.output)
}
