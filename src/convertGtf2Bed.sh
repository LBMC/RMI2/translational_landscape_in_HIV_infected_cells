#!/bin/bash

awk '{ if ($0 ~ "transcript_id") print $0; else print $0" transcript_id \"\";"; }' data/annotation/gencode.v28.annotation_NL4.3_forRiboCode_updated.gtf | gtf2bed - > gencode.v28.annotation_NL4.3_forRiboCode_updated.bed
